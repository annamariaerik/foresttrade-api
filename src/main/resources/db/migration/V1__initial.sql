CREATE SEQUENCE user_seq start 1 increment 1;

CREATE TABLE IF NOT EXISTS user_info (
   id bigint PRIMARY KEY DEFAULT nextval('user_seq'),
   username character varying(50) NOT NULL UNIQUE,
   password character varying(200) NOT NULL,
   phone character varying(50) NOT NULL UNIQUE
);
CREATE SEQUENCE offer_seq start 1 increment 1;

CREATE TABLE IF NOT EXISTS offer_info (
   offer_id bigint PRIMARY KEY DEFAULT nextval('offer_seq'),
   user_id bigint NOT NULL,
   user_phone character varying(50) NOT NULL,
   object character varying(50) NOT NULL,
   location character varying(50) NOT NULL,
   buyer_email character varying(50),
   buyer_phone character varying(50) NOT NULL,
   buyer_name character varying(50) NOT NULL,
   description character varying(50) NOT NULL,
   offer_type character varying(50) NOT NULL,
   created TIMESTAMP NOT NULL DEFAULT now(),
   price integer NOT NULL,
  CONSTRAINT offer_info_user_id_FK FOREIGN KEY (user_id) REFERENCES user_info (id)
);

INSERT INTO user_info (username, password, phone)
VALUES ('admin', '$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei', '500');
