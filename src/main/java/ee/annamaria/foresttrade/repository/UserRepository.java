package ee.annamaria.foresttrade.repository;

import ee.annamaria.foresttrade.domain.UserInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserInfo, Long> {
    List<UserInfo> findByPhone(String phone);
    List<UserInfo> findByUsername(String username);
}