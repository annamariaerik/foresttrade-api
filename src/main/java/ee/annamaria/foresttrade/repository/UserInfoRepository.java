package ee.annamaria.foresttrade.repository;

import ee.annamaria.foresttrade.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserInfoRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserInfo getUserByUsername(String username) {
        List<UserInfo> user = jdbcTemplate.query(
                "select * from user_info where username = ?",
                new Object[]{username},
                (rs, rowNum) -> new UserInfo (rs.getLong("id"), rs.getString("username"), rs.getString("password"), rs.getString("phone"))
        );
        return user.size() > 0 ? user.get(0) : null;
    }

    public void updateUser(UserInfo user) {
        if(user.getPassword() == null ) {
            jdbcTemplate.update("UPDATE user_info SET phone = ? WHERE username = ?", user.getPhone(), user.getUsername());
        } else {
            jdbcTemplate.update("UPDATE user_info SET password = ?, phone = ? WHERE username = ?",
                    passwordEncoder.encode(user.getPassword()), user.getPhone(), user.getUsername());
        }
    }
}
