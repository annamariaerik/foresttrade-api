package ee.annamaria.foresttrade.repository;

import ee.annamaria.foresttrade.domain.OfferInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends CrudRepository<OfferInfo, Long> {
    List<OfferInfo> findByUserId(Long userId);
}