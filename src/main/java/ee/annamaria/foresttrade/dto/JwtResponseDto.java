package ee.annamaria.foresttrade.dto;

import lombok.Data;

@Data
public class JwtResponseDto {
    private final long id;
    private final String username;
    private final String token;
    private final String phone;
}
