package ee.annamaria.foresttrade.dto;

import lombok.Data;

@Data
public class OfferDto {
    private String object;
    private String userPhone;
    private int price;
    private String description;
    private String buyerName;
    private String buyerPhone;
    private String buyerEmail;
    private String offerType;
    private String location;
}
