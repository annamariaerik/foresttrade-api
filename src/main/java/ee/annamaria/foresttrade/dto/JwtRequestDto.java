package ee.annamaria.foresttrade.dto;

import lombok.Data;

@Data
public class JwtRequestDto {
    private String username;
    private String password;
    private String phone;
}
