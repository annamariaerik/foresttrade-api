package ee.annamaria.foresttrade.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GenericResponseDto {
    private List<String> errors = new ArrayList<>();
}