package ee.annamaria.foresttrade.dto;

import lombok.Data;

@Data
public class UserRegistrationDto {
    public String username;
    public String password;
    public String phone;
}
