package ee.annamaria.foresttrade.service;

import ee.annamaria.foresttrade.domain.UserInfo;
import ee.annamaria.foresttrade.dto.GenericResponseDto;
import ee.annamaria.foresttrade.dto.JwtRequestDto;
import ee.annamaria.foresttrade.dto.JwtResponseDto;
import ee.annamaria.foresttrade.dto.UserRegistrationDto;
import ee.annamaria.foresttrade.repository.UserInfoRepository;
import ee.annamaria.foresttrade.repository.UserRepository;
import ee.annamaria.foresttrade.security.JwtTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final UserInfo userDetails = userInfoRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token, userDetails.getPhone());
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        UserInfo user = new UserInfo(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()), userRegistration.getPhone());
        if (!userExists(userRegistration.getUsername())) {
            addUser(user);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }

    public String phoneExists(String phone) {
        int count = userRepository.findByPhone(phone).size();
        if(count > 0) {
            return phone;
        } else {
            return "null";
        }
    }

    private boolean userExists(String username) {
        int count = userRepository.findByUsername(username).size();
        return count > 0;
    }

    private void addUser(UserInfo user) {
        userRepository.save(user);
    }
}
