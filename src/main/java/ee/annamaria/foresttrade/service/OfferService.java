package ee.annamaria.foresttrade.service;

import ee.annamaria.foresttrade.domain.OfferInfo;
import ee.annamaria.foresttrade.domain.UserInfo;
import ee.annamaria.foresttrade.dto.OfferDto;
import ee.annamaria.foresttrade.repository.OfferRepository;
import ee.annamaria.foresttrade.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferService {
    @Autowired
    OfferRepository offerRepository;

    @Autowired
    UserRepository userRepository;

    public void addOffer(OfferDto offerDto) {
        offerRepository.save(convertOfferDtoToOfferInfo(offerDto));
    }

    public OfferInfo convertOfferDtoToOfferInfo(OfferDto offerDto) {
        OfferInfo offerInfo = new OfferInfo();

        offerInfo.setBuyerEmail(offerDto.getBuyerEmail());
        offerInfo.setBuyerName(offerDto.getBuyerName());
        offerInfo.setBuyerPhone(offerDto.getBuyerPhone());
        offerInfo.setObject(offerDto.getObject());
        offerInfo.setLocation(offerDto.getLocation());
        offerInfo.setPrice(offerDto.getPrice());
        offerInfo.setDescription(offerDto.getDescription());
        offerInfo.setOfferType(offerDto.getOfferType());
        offerInfo.setUserPhone(offerDto.getUserPhone());
        offerInfo.setUserId(getUserId(offerDto));

        return offerInfo;
    }

    public Long getUserId(OfferDto offerDto) {
        List<UserInfo> userList = userRepository.findByPhone(offerDto.getUserPhone());
        Long userId = null;
        for (UserInfo user : userList) {
            userId = user.getId();
        }
        return userId;
    }
}
