package ee.annamaria.foresttrade.controller;

import ee.annamaria.foresttrade.domain.OfferInfo;
import ee.annamaria.foresttrade.dto.OfferDto;
import ee.annamaria.foresttrade.repository.OfferRepository;
import ee.annamaria.foresttrade.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@CrossOrigin("*")
@RequestMapping("/offer")
public class OfferController {
    @Autowired
    OfferRepository offerRepository;

    @Autowired
    OfferService offerService;

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public List<OfferInfo> fetchOffers(@RequestParam(value = "userId", required = false) Long userId) {
        return offerRepository.findByUserId(userId);
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    public void addOffer(@RequestBody OfferDto offerDto) {
        offerService.addOffer(offerDto);
    }
}