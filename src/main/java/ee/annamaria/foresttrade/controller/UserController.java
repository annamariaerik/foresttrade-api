package ee.annamaria.foresttrade.controller;

import ee.annamaria.foresttrade.domain.UserInfo;
import ee.annamaria.foresttrade.dto.GenericResponseDto;
import ee.annamaria.foresttrade.dto.JwtRequestDto;
import ee.annamaria.foresttrade.dto.JwtResponseDto;
import ee.annamaria.foresttrade.dto.UserRegistrationDto;
import ee.annamaria.foresttrade.repository.UserInfoRepository;
import ee.annamaria.foresttrade.repository.UserRepository;
import ee.annamaria.foresttrade.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public UserInfo getUserInfo(@RequestParam(value = "username") String username) {
        return userInfoRepository.getUserByUsername(username);
    }

    @RequestMapping(value = "/addoffer", method = RequestMethod.GET)
    public String phoneExists(@RequestParam(value = "phone") String phone) {
        return userService.phoneExists(phone);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public GenericResponseDto registerUser(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto requestDto) throws Exception {
        return userService.authenticate(requestDto);
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public void updateUserInfo(@RequestBody UserInfo userInfo) {
        userInfoRepository.updateUser(userInfo);
    }
}