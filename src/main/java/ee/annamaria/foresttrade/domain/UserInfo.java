package ee.annamaria.foresttrade.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@SequenceGenerator(name = "id_seq", sequenceName = "user_seq", allocationSize = 1)
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private long id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String phone;

    public UserInfo(long id, String username, String password, String phone) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.phone = phone;
    }

    public UserInfo(String username, String password, String phone) {
        this.username = username;
        this.password = password;
        this.phone = phone;
    }
}
