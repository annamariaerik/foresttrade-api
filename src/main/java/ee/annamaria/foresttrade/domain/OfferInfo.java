package ee.annamaria.foresttrade.domain;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(name = "id_seq", sequenceName = "offer_seq", allocationSize = 1)
public class OfferInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private long offerId;

    @Column
    private long userId;

    @Column
    private String object;

    @Column
    private String userPhone;

    @CreatedDate
    @Column
    private Date created;

    @Column
    private int price;

    @Column
    private String description;

    @Column
    private String buyerName;

    @Column
    private String buyerPhone;

    @Column
    private String buyerEmail;

    @Column
    private String offerType;

    @Column
    private String location;
}